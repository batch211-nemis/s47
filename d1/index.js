console.log(document);
console.log(document.querySelector("#txt-first-name"));
//result - element from HTML

/*
	document - refers to the whole webpage
	querySelector - used to select a specific element inside the html tag (HTML ELEMENT);
	-takes a string input that is formatted like CSS Selector
	-can select eleemnt regardless if the string is an id, a calss, or a tag as long as the element is exixting in the webpage
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
	


	addEventListener - function that lets webpage to listen to the events performed by the user.
		takes two arguments
			-string - the ebvent to which the HTML will listen, these are pre-determined
			- function - executed by the element once the event (first argument) is triggered
*/

txtFirstName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})


//MINI-ACTIVITY & ACTIVITY
txtLastName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
})

txtLastName.addEventListener('keyup', (event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})


